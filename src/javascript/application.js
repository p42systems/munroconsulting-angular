getUrlParameter = function(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');
  for (var i = 0; i < sURLVariables.length; i++)
  {
    var sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] == sParam)
    {
      return sParameterName[1];
    }
  }
}

window.downloadFile = function (sUrl) {

  window.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
  window.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;

    //iOS devices do not support downloading. We have to inform user about this.
    if (/(iP)/g.test(navigator.userAgent)) {
        alert('Your device does not support files downloading. Please try again in desktop browser.');
        return false;
    }

    //If in Chrome or Safari - download via virtual link click
    if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;

        if (link.download !== undefined) {
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
            link.download = fileName;
        }

        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click', true, true);
            link.dispatchEvent(e);
            return true;
        }
    }

    // Force file download (whether supported by server).
    if (sUrl.indexOf('?') === -1) {
        sUrl += '?download';
    }

    window.open(sUrl, '_self');
    return true;
}



angular.module('app', ['ui.bootstrap'])
  .run(['$rootScope', '$http', 'API', function($rootScope, $http, API){
      // $rootScope.json_endpoint = "http://munroapi.p42.ca";
      $rootScope.json_endpoint = "http://localhost:5000";
      $rootScope.customer_id = getUrlParameter('customer');

      $rootScope.allocation_sorter = 'service';

      $rootScope.show_totals = false;


      if(!$rootScope.customer_id){

        $('.noclient').removeClass('hidden');

        // Create Customer to show in select link
        $rootScope.customer = {name: "Client Select"};

        return;
      }

      // Get customers data
      $http.get( $rootScope.json_endpoint + '/customer/' + $rootScope.customer_id)
        .success(function(data){
          $rootScope.customer = data.customer;
        });

      $rootScope.select_fields = {};

      get_services = $http.get( $rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/services')
        .success(function(data){
          $rootScope.select_fields.services = data;
        });
      get_jobs = $http.get( $rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/jobs')
        .success(function(data){
          $rootScope.select_fields.jobs = data;
        })

      $.when(get_services, get_jobs).done(function(){
        $rootScope.allocations = [];
        get_allocations = $http.get($rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/allocations')
          .success(function(data){
            $.each(data, function(idx, item){
              item.select_fields = angular.copy($rootScope.select_fields)
              API.setup_select_fields(item);
            });
            $rootScope.allocations = data;

            $rootScope.allocations.push({
              select_fields: angular.copy($rootScope.select_fields)
            });
          });
        $('.allocations').removeClass('hidden');
      })


      $rootScope.getTotal = function(activity, type){
        var ret = 0;
        if(typeof activity === 'undefined') return '';
        $.each($rootScope.allocations, function(idx, item){
          if(item.activity && activity && activity.id == item.activity.id){
            ret += parseFloat( (item[type][$rootScope.year] || 0) )
          }
        })
        return ret;
      }


      $rootScope.getCalcTotal = function(activity, type){
        var ret = 0;
        if(typeof activity === 'undefined') return '';
        $.each($rootScope.allocations, function(idx, item){
          if(item.activity && activity && activity.id == item.activity.id){
            ret += ( parseFloat(item[type][$rootScope.year]) || 0) * (( parseFloat(item[type + "_percent"][$rootScope.year]) || 0) / 100);
          }
        })
        return ret;
      }

      $rootScope.getSorter = function(a){
        // TODO: Change sorting to called function not orderBy filter
        if (typeof a[$rootScope.allocation_sorter] == 'undefined'){
          return "ZZZZZZZZZZZZZZZZZ"
        }
        return a[$rootScope.allocation_sorter].description;
      }

      $rootScope.sortAllocations = function(type){
        $rootScope.allocation_sorter = type;
        // $rootScope.$apply(function(){
          $rootScope.allocations.sort(function (a, b) {
            if (typeof a[type] == 'undefined'){
              return 1;
            }
            if (typeof b[type] == 'undefined'){
              return -1;
            }
            if (a[type].description > b[type].description) {
              return 1;
            }
            if (a[type].description < b[type].description) {
              return -1;
            }
            // a must be equal to b
            return 0;
          })
        // })
      }



      $rootScope.year = new Date().getFullYear();
    }])
  .factory('API', ['$http', '$rootScope', function($http, $rootScope){
      return {
        saveAllocation: function(indata){
          $http.post($rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/allocation', indata)
            .success(function(data){
              $.each(data, function(idx, item){
                if(typeof indata.ids == 'undefined'){
                  indata.ids = {}
                }
                indata.ids[item.fiscal_year] = item.id
              })
              // indata.id = data.id;
            })
        },
        setup_select_fields: function(indata){
          $.each(indata.select_fields.services, function(){
            if(indata.service.id == this.id){
              indata.service = this;
            }
          });
          $.each(indata.select_fields.jobs, function(){
            if(indata.job.id == this.id){
              indata.job = this;
            }
          });
          $http.get( $rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/service/' + indata.service.id)
            .success(function(data){
              indata.select_fields.sub_services = data.sub_services;
            })
            .then(function(){
              $.each(indata.select_fields.sub_services, function(){
                if(indata.sub_service.id == this.id){
                  indata.sub_service = this;
                }
              });
            });
          $http.get( $rootScope.json_endpoint + '/customer/' + $rootScope.customer_id + '/job/' + indata.job.id)
            .success(function(data){
              indata.select_fields.activities = data.activities;
            })
            .then(function(){
              $.each(indata.select_fields.activities, function(){
                if(indata.activity.id == this.id){
                  indata.activity = this;
                }
              });
            });
        }
      }
    }])
  .factory('FileUploader', ['$http', '$window', function($http, $window){
      return {
        uploadFileToUrl: function(options){
          var fd = new FormData();
          fd.append('file', options.file);
          fd.append('data', JSON.stringify(options.data));
          $http.post(options.uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
          })
          .success(function(data){
            $window.location.href = "/?customer=" + data.customer
          })
        }
      }
    }])
  .directive('allocation', ['$http', '$rootScope', 'API', function($http, $rootScope, API){
      return {
        restrict: 'A',
        scope: '=',
        // replace: true,
        templateUrl: "templates/allocation_row.html",
        link: function(scope, element, attrs){

          element.on('click', function(event){
            if(!scope.edit){
              scope.edit = true;
              scope.context.isDirty = true;
              scope.$parent.$broadcast('SAVE', scope.context.$$hashKey);
              scope.add_new();
              scope.$apply();
            }
          })

          scope.$on("SAVE", function(event, message){
            if(scope.context.$$hashKey !== message){
              scope.edit = false;
              scope.save();
              if(!scope.$$phase) {
                scope.$apply();
              }
            }
          })


          scope.add_new = function(){
            last_a = scope.$parent.allocations[scope.$parent.allocations.length - 1];
            if(last_a.service || last_a.$$hashKey == scope.context.$$hashKey){
              scope.$parent.allocations.push({
                select_fields: scope.$parent.select_fields
              });
            }
          }

          scope.save = function(){

            if(scope.context.isDirty){
              API.saveAllocation(scope.context);
              scope.context.isDirty = false;
            }

          }

          scope.get_expense = function(year){
            if(!scope.context.expense_percent
              || !scope.context.expense
              || !scope.context.expense[year]
              || !scope.context.expense_percent[year]) return 0;
            return scope.context.expense[year] * (scope.context.expense_percent[year] / 100);
          }
          scope.get_revenue = function(year){
            if(!scope.context.revenue_percent
              || !scope.context.revenue
              || !scope.context.revenue[year]
              || !scope.context.revenue_percent[year]) return 0;
            return scope.context.revenue[year] * (scope.context.revenue_percent[year] / 100);
          }

          scope.get_net = function(year){
            return scope.get_revenue(year) - scope.get_expense(year);
          }

          scope.set_field = function(type){

            if(type == 'service'){
              $http.get( scope.json_endpoint + '/customer/' + scope.customer_id + '/service/' + scope.context.service.id)
                .success(function(data){
                  scope.context.select_fields.sub_services = data.sub_services;
                });
            }

            if(type == 'job'){
              $http.get( scope.json_endpoint + '/customer/' + scope.customer_id + '/job/' + scope.context.job.id)
                .success(function(data){
                  scope.context.select_fields.activities = data.activities;
                });
            }

            if(type == 'activity'){
              $http.get( scope.json_endpoint + '/customer/' + scope.customer_id + '/activity/' + scope.context.activity.id + '/finances')
                .success(function(data){
                  scope.context.revenue = data.revenue
                  scope.context.expense = data.expense
                })
            }

          }

        }
      }
    }])
  .directive('newclient', ['$http', '$modal', 'FileUploader', function($http, $modal, FileUploader){
      return {
        restrict: 'A',
        scope: { form: '=' },
        link: function(scope, element, attrs){
          element.on('click', function(event){
            event.preventDefault();

            $modal.open({
              windowClass: "newclient-modal",
              templateUrl: "templates/new_customer.html",
              controller: ['$rootScope', '$scope', '$modalInstance', function($rootScope, $scope, $modalInstance){

                $scope.cancel = function(){
                  $modalInstance.close();
                }

                $scope.save = function(){
                  file_input = $('.newclient-modal #inputDatabaseFile');
                  FileUploader.uploadFileToUrl({
                    file: file_input[0].files[0],
                    uploadUrl: $rootScope.json_endpoint + '/customer',
                    data: $scope.form
                  });
                  $modalInstance.close();
                }

              }]
            })
          })
        }
      }
    }])
  .directive('navbarButton', ['$http', '$modal', function($http, $modal){
      return {
        restrict: 'E',
        scope: '=',
        link: function(scope, element){
          element.addClass('navbar-directive');

          scope.download_csv = function(){
            downloadFile(scope.json_endpoint + '/customer/' + scope.customer_id + '/client.csv')
          }

          scope.toggle_totals = function(){
            scope.show_totals = !scope.show_totals
          }

          scope.select_client = function(){

            $http.get(scope.json_endpoint + '/customers')
              .success(function(data){
                scope.customers = data.customers
              })
              .then(function(){
                $modal.open({
                  templateUrl: "templates/customer_select.html",
                  controller: ['$scope', '$modalInstance', function($scope, $modalInstance){

                    $scope.cancel = function(){
                      $modalInstance.close();
                    }

                    $scope.delete_customer = function(customer){

                      if(confirm("Are you sure you want to remove '" + customer.id + "'?\nWarning cannot be undone!")){
                        $http.delete(scope.json_endpoint + '/customer/' + customer.id)
                          .success(function(data){
                            index = scope.customers.indexOf(customer);
                            if (index > -1) {
                              scope.customers.splice(index, 1);
                            }
                          });
                      }
                    }

                  }]
                })
              })

          }

          scope.copy_percentages = function(){
            $.each(scope.allocations, function(idx, item){
              if(item.activity){
                var current = {
                  "expense_percent": item.expense_percent[scope.year],
                  "revenue_percent": item.revenue_percent[scope.year]
                }
                $.each(scope.customer.years, function(idy, year){
                  item.expense_percent[year] = current.expense_percent;
                  item.revenue_percent[year] = current.revenue_percent;
                  item.isDirty = true;
                })
              }
            })

            scope.$broadcast('SAVE',"");
          }

        }
      }
    }])
  .directive('year', [function(){
      return {
        restrict: 'E',
        scope: '@',
        link: function(scope, element, attrs){
          element.css('width', 100)
          element.on('click', function(){
            if(typeof attrs.next !== 'undefined'){
              scope.year = scope.year + 1;
            }
            if(typeof attrs.prev !== 'undefined'){
              scope.year = scope.year - 1;
            }
            scope.$apply();
          })
        }
      }
    }])
  .directive('body', ['$rootScope', function($rootScope){
      return {
        restrict: 'A',
        scope: {},
        link: function(scope, element){
          element.on('click', function(event){
            save_triggers = ["BODY", "TH", "DIV"]
            tag = event.target.tagName;
            if(save_triggers.indexOf(tag) > -1){
              $rootScope.$broadcast('SAVE',"");
            }
          })
        }
      }
    }])
